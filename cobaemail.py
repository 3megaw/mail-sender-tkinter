from tkinter import *
import smtplib

master       = Tk()
master.title = 'App Kirim Email'

def kirim():
    try:
        email = temp_email.get()
        sandi = temp_sandi.get()
        ke = temp_penerima.get()
        subjek = temp_subjek.get()
        isi = temp_isi.get()
        pesan = 'Subject: {}\n\n{}'.format(subjek, isi)
        server   = smtplib.SMTP('smtp.gmail.com',587)
        server.ehlo()
        server.starttls()
        server.login(email, sandi)
        server.sendmail(email,ke,pesan)
        notif.config(text="Email terkirim", fg="green")
    except Exception as e:
        notif.config(text="Error", fg="red")
        print(e)

Label(master, text="Email").grid(row=0,sticky=W, padx=5)
Label(master, text="Password").grid(row=1,sticky=W, padx=5)
Label(master, text="To").grid(row=2,sticky=W, padx=5)
Label(master, text="Subject").grid(row=3,sticky=W, padx=5)
Label(master, text="Body").grid(row=4,sticky=W, padx=5)
notif = Label(master, text="",fg="red")
notif.grid(row=5,sticky=S)

temp_email = StringVar()
temp_sandi = StringVar()
temp_penerima = StringVar()
temp_subjek = StringVar()
temp_isi = StringVar()

kolomEmail = Entry(master, textvariable = temp_email)
kolomEmail.grid(row=0,column=1)
kolomSandi = Entry(master, show="*", textvariable = temp_sandi)
kolomSandi.grid(row=1,column=1)
kolomPenerima  = Entry(master, textvariable = temp_penerima)
kolomPenerima.grid(row=2,column=1)
kolomSubjek = Entry(master, textvariable = temp_subjek)
kolomSubjek.grid(row=3,column=1)
kolomIsi= Entry(master, textvariable = temp_isi)
kolomIsi.grid(row=4,column=1)

Button(master, text = "kirim", command = kirim).grid(row=6,column=1, sticky=E, padx=10, pady=10)

master.mainloop()